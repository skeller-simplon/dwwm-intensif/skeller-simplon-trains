
export class Bread {
    /**
     * 
     * @param {String} type 
     * @param {Number} weight 
     */
    constructor(type, weight) {
        this.cooked = false;
        this.type = type;
        this.weight = weight
    }

    /**
     * 
     * @param {Number} temp La température de cuisson
     * @returns {Boolean}
     */
    cook(temp) {
        if (this.cooked === false) {
            if (temp > 200 && temp < 400) {
                this.weight -= (this.weight / 10);
                this.cooked = true;
                return true;
            }
            return false;
        }
    }
}