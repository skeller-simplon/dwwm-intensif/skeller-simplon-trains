import { Bread } from "./Bread";

export class Baker {
    /**
     * 
     * @param {String} name 
     * @param {Number} floorSupply 
     */
    constructor(name, floorSupply) {
        this.name = name;
        this.floorSupply = floorSupply;
    }
    bakeBreads(quantity) {
        let breads = [];
        for (let i = 0; i < quantity && this.floorSupply > 0; i++) {
            let pain =new Bread('Baguette', Math.floor(Math.random() * 50 + 150))
            pain.cook(300);
            breads.push(pain)
            this.floorSupply -= 1;
        }
        return breads;
    }
}