console.log('Hello')
import { Bread } from "./Bread";
import { FrenchPerson } from "./FrenchPerson";
import { Baker } from "./Baker";

let baguette = new Bread('baguette', 230)
let michel = new FrenchPerson(3000, 'Jean-Pierre')

let i = 0;

while (michel.hunger > 0) {
    michel.eat(baguette);
    i++;
}
console.log(michel.name + ' a mangé ' + i + ' ' + baguette.type);


let thierryLeBoulanger = new Baker('Thierry', 50);

let fourneeDuMatin = thierryLeBoulanger.bakeBreads(30);

console.log(fourneeDuMatin);
