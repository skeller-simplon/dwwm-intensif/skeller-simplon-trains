export class FrenchPerson {

    /**
     * 
     * @param {Number} hunger 
     * @param {String} name 
     */
    constructor(hunger, name) {
        this.hunger = hunger;
        this.name = name;
    }
    /**
     * 
     * @param {Bread} michel Un pain connard
     */
    eat(pain){
        this.hunger -= pain.weight;
    }
}