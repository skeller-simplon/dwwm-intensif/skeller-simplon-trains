/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Baker.js":
/*!**********************!*\
  !*** ./src/Baker.js ***!
  \**********************/
/*! exports provided: Baker */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Baker\", function() { return Baker; });\n/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bread */ \"./src/Bread.js\");\n\n\nclass Baker {\n    /**\n     * \n     * @param {String} name \n     * @param {Number} floorSupply \n     */\n    constructor(name, floorSupply) {\n        this.name = name;\n        this.floorSupply = floorSupply;\n    }\n    bakeBreads(quantity) {\n        let breads = [];\n        for (let i = 0; i < quantity && this.floorSupply > 0; i++) {\n            let pain =new _Bread__WEBPACK_IMPORTED_MODULE_0__[\"Bread\"]('Baguette', Math.floor(Math.random() * 50 + 150))\n            pain.cook(300);\n            breads.push(pain)\n            this.floorSupply -= 1;\n        }\n        return breads;\n    }\n}\n\n//# sourceURL=webpack:///./src/Baker.js?");

/***/ }),

/***/ "./src/Bread.js":
/*!**********************!*\
  !*** ./src/Bread.js ***!
  \**********************/
/*! exports provided: Bread */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Bread\", function() { return Bread; });\n\nclass Bread {\n    /**\n     * \n     * @param {String} type \n     * @param {Number} weight \n     */\n    constructor(type, weight) {\n        this.cooked = false;\n        this.type = type;\n        this.weight = weight\n    }\n\n    /**\n     * \n     * @param {Number} temp La température de cuisson\n     * @returns {Boolean}\n     */\n    cook(temp) {\n        if (this.cooked === false) {\n            if (temp > 200 && temp < 400) {\n                this.weight -= (this.weight / 10);\n                this.cooked = true;\n                return true;\n            }\n            return false;\n        }\n    }\n}\n\n//# sourceURL=webpack:///./src/Bread.js?");

/***/ }),

/***/ "./src/FrenchPerson.js":
/*!*****************************!*\
  !*** ./src/FrenchPerson.js ***!
  \*****************************/
/*! exports provided: FrenchPerson */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrenchPerson\", function() { return FrenchPerson; });\nclass FrenchPerson {\n\n    /**\n     * \n     * @param {Number} hunger \n     * @param {String} name \n     */\n    constructor(hunger, name) {\n        this.hunger = hunger;\n        this.name = name;\n    }\n    /**\n     * \n     * @param {Bread} michel Un pain connard\n     */\n    eat(pain){\n        this.hunger -= pain.weight;\n    }\n}\n\n//# sourceURL=webpack:///./src/FrenchPerson.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Bread__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Bread */ \"./src/Bread.js\");\n/* harmony import */ var _FrenchPerson__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FrenchPerson */ \"./src/FrenchPerson.js\");\n/* harmony import */ var _Baker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Baker */ \"./src/Baker.js\");\nconsole.log('Hello')\n\n\n\n\nlet baguette = new _Bread__WEBPACK_IMPORTED_MODULE_0__[\"Bread\"]('baguette', 230)\nlet michel = new _FrenchPerson__WEBPACK_IMPORTED_MODULE_1__[\"FrenchPerson\"](3000, 'Jean-Pierre')\n\nlet i = 0;\n\nwhile (michel.hunger > 0) {\n    michel.eat(baguette);\n    i++;\n}\nconsole.log(michel.name + ' a mangé ' + i + ' ' + baguette.type);\n\n\nlet thierryLeBoulanger = new _Baker__WEBPACK_IMPORTED_MODULE_2__[\"Baker\"]('Thierry', 50);\n\nlet fourneeDuMatin = thierryLeBoulanger.bakeBreads(30);\n\nconsole.log(fourneeDuMatin);\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });