import { mount, shallowMount } from '@vue/test-utils';
import AddTodo from '@/components/AddTodo';

test('AddTodo', () =>{
    const wrapper = shallowMount(AddTodo);
    expect(wrapper.find("input").exists()).toBe(true);
})
