import { mount, shallowMount } from '@vue/test-utils';
import DoneList from '@/components/DoneListView';

test('DoneList', () => {
    const wrapper = shallowMount(DoneList);
    expect(wrapper.find("h1").exists()).toBe(true);
    expect(wrapper.find('h1').text()).toEqual('Already Done')
})

describe('DoneList', () => {
    it('restitue un message et répond correctement à la saisie de l\'utilisateur', () => {
        const wrapper = shallowMount(DoneList, {
            data: {
                todos: [
                    {
                        id: 1,
                        title: 'Un Truc fait'
                    },
                    {
                        id: 2,
                        title: 'Un autre truc fait'
                    }, {
                        id: 3,
                        title: 'Un dernier Truc fait'
                    }
                ]
            }
        })

        // vérifie si le message est restitué
        expect(wrapper.find('.list-unstyled').contains('li')).toBe(true)
        // vérifie que `error` est rendue
        // expect(wrapper.find('.error').exists()).toBeTruthy()

        // met à jour `username` et vérifie que `error` n'est plus rendu
        // wrapper.setData({ username: 'Lachlan' })
        // expect(wrapper.find('.error').exists()).toBeFalsy()
    })
})