<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;
use App\Entity\Address;
use App\Form\AddressType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\UserRepository;

/**
 * @Route("/api/address")
 */
class ApiAddressController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
    /**
     * @Route(methods="GET")
     */
    public function getAddresses(){
        return new JsonResponse($this->serializer->serialize(
            $this->getUser()->getAddresses(), 'json')
        );
    }
    /**
     * @Route(methods="POST")
     */
    public function addAddress(Request $request, ObjectManager $manager, UserRepository $userRepository)
    {
        $address = new Address();
        $form = $this->createForm(AddressType::class, $address);
        $form->submit(json_decode($request->getContent(), true));

        // $user = $userRepository->findOneBy(["id" => 1]);
        if($form->isSubmitted() && $form->isValid()) {
            // return $this->json($request->getContent(), 400);
            $address->setUser($this->getUser());
            $manager->persist($address);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($address, 'json'),201, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }
}
