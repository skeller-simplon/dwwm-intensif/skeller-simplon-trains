<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

use JMS\Serializer\SerializerInterface;
use App\Repository\DeviceRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use App\Form\DeviceType;

abstract class AbstractRestController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    private $entity;
    private $type;
    /**
     * On injecte le JMS\Serializer dans le constructeur, car on en aura
     * besoin dans toutes les méthodes de ce contrôleur
     */
    public function __construct(SerializerInterface $serializer, string $entity, string $type) {
        $this->serializer = $serializer;
        $this->entity = $entity;
        $this->type = $type;
        
    }
    /**
     * @Route(methods="GET")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository($this->entity);
        //On va chercher les devices comme d'hab
        $entities = $repository->findAll();
        //On les convertit en json avec le jms serializer
        $json = $this->serializer->serialize($entities, 'json');

        // return new Response(json_encode(["ga" => "bloup"]), 200, ['Content-Type'=> 'application/json']);
        // return $this->json(["ga" => "bloup"]);

        //On envoie le json dans une JsonResponse avec un code http à 200 (success)
        //pas de header Http, et en lui disant que ce qu'on lui donne c'est déjà du json
        return new JsonResponse($json, 200, [], true);

    }
    /**
     * @Route(methods="POST")
     */
    public function add(Request $request, ObjectManager $manager) {
        $device = new $this->entity();        
        $form = $this->createForm($this->type, $device);

        $form->submit(json_decode($request->getContent(), true));

        if($form->isSubmitted() && $form->isValid()) {
            $this->beforePersist($device);
            $manager->persist($device);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($device, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);

    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function one(int $id) {
        $repository = $this->getDoctrine()->getRepository($this->entity);
        $entity = $repository->find($id);
        if($entity === null) {
            return $this->json('', 404);
        }
        
        return new JsonResponse($this->serializer->serialize($entity, 'json'), 200, [], true);
        
        
    }
    
    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function remove(int $id, ObjectManager $manager) {
        $repository = $this->getDoctrine()->getRepository($this->entity);
        
        $entity = $repository->find($id);
        if($entity === null) {
            return $this->json('', 404);
        }
        $manager->remove($entity);
        $manager->flush();
        return $this->json('', 204);
    }

    /**
     * @Route("/{id}", methods="PATCH")
     */
    public function update(int $id, ObjectManager $manager, Request $request) {
        $repository = $this->getDoctrine()->getRepository($this->entity);
        $entity = $repository->find($id);
        if($entity === null) {
            return $this->json('', 404);
        }
        $form = $this->createForm($this->type, $entity);
        $form->submit(json_decode($request->getContent(), true), false);
        if($form->isSubmitted() && $form->isValid()) {
            $manager->flush();
            return new JsonResponse($this->serializer->serialize($entity, 'json'), 200, [], true);
        }
        return $this->json($form->getErrors(true), 400);
    }
    protected function beforePersist($entity){ }
}
