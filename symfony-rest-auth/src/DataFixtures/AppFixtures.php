<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Promo;
use App\Entity\Address;
use App\Entity\User;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadUser($manager);
        $this->loadPromos($manager);
        $this->loadAdresses($manager);
        $manager->flush();
    }
    private function loadPromos(ObjectManager $objectManager){
        for ($i=0; $i < 10; $i++) { 
            $promo = new Promo();
            $promo->setLabel("Promo ". $i);
            $promo->setSpecialisation("Specialisation");
            $objectManager->persist($promo);
        }
    }
    private function loadUser(ObjectManager $objectManager){
        for ($i=0; $i < 20; $i++) { 
            $user = new User();
            $user->setAge($i);
            $user->setEmail('mail'.$i.'@mail.com');
            $user->setPassword('pass');
            $this->addReference('user'.$i, $user);
            $objectManager->persist($user);
        }
    }
    private function loadAdresses(ObjectManager $objectManager){
        for ($i=0; $i < 10; $i++) { 
            $address = new Address();
            $address->setCity('city '. $i);
            $address->setNumber(''.$i);
            $address->setStreet('street '.$i);
            $address->setUser($this->getReference('user'.rand(0,15)));
            $objectManager->persist($address);
        }
    }

}

