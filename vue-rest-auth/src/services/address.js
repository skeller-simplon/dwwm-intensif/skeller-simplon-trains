import Axios from "axios";

export class Address{
    constructor(){
        this.url = 'http://localhost:8000/api/address';
    }

    async findAll(){
        $response =  await Axios.get(this.url);
        return $response.data;
    }
}