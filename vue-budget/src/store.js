import Vue from 'vue'
import Vuex from 'vuex'
import Axios from 'axios';

Vue.use(Vuex)

const apiUrl = 'http://localhost:8000';

export default new Vuex.Store({
  state: {
    account: {},
    operations: [],
    categories: []
  },
  mutations: {
    CHANGE_ACCOUNT(state, account) {
      state.account = account;
    },
    ADD_OPERATION(state, operation) {
      state.operations.push(operation);
    },
    REMOVE_OPERATION(state, operation) {
      state.operations = state.operations.filter(item => item.id !== operation.id)
    },
    MODIFY_OPERATIONS(state, operationList) {
      state.operations = operationList;
    },
    CHANGE_CATEGORIES(state, categories){
      state.categories = categories;
    }
  },
  actions: {
    async fetchAccount({ commit }) {
      let response = await Axios.get(apiUrl + "/api/account/1");
      console.log(response.data);
      commit('CHANGE_ACCOUNT', response.data);
    },
    async fetchOperations({ commit }) {
      // if (this.account === null) {
        // this.fetchAccount({commit});
      // }
      let response = await Axios.get(apiUrl + '/api/operation');
      commit('MODIFY_OPERATIONS', response.data);
    },
    async addOperation({ commit }, operation) {
      let response = Axios.post(apiUrl + '/api/operation', operation);
      commit('ADD_OPERATION', response.data);
    },
    deleteOperations({ commit }, operation) {
      Axios.delete(apiUrl + '/api/operation/' + operation.id);
      commit('REMOVE_OPERATION', operation);
    },
    async fetchCategories({ commit }){
      let response = await Axios.get(apiUrl + "/api/category")
      commit('CHANGE_CATEGORIES', response.data);

    }
  },
  strict: true
})
