import { Router } from "express";

export const first = Router();

// first.get('/', (req, resp) => {
// resp.json({ msg: "coucou" })
// });

let list: String[] = ['ga', 'bu', 'zo']

first.get('/list', (req, resp) => {
    resp.json({
        list
    });
});

first.post('/list', (req, resp) => {
    if (!req.body.item){
        return resp.status(400).json({msg: "You should have an item property"})
    }
    list.push(req.body.item);
    resp.json({msg:'done'})
});