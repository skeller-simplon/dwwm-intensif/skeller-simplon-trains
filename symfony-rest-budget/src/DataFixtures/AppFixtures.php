<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Account;
use App\Entity\Category;
use App\Entity\Operation;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $account = new Account();
        $account->setLabel('Fake Account')
        ->setIban('A231QSDFQ1231VAQDQFSD');

        $manager->persist($account);

        $category = new Category();
        $category->setLabel('Fake category');
        $manager->persist($category);

        for ($i=0; $i < 5; $i++) { 
            $operation = new Operation();
            $operation->setAccount($account)
            ->setAmount(rand(-100, 100))
            ->setCategory($category)
            ->setDate(new \DateTime())
            ->setDescription("Fake Operation ".$i);
            $manager->persist($operation);
        }

        $manager->flush();
    }
}
