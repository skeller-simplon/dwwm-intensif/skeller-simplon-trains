<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use JMS\Serializer\SerializerInterface;
use App\Form\CategoryType;

/**
 * @Route("/api/category", name="category")
 */
class ApiCategoryController extends AbstractRestController {

    public function __construct(SerializerInterface $serializer) {
        parent::__construct($serializer, Category::class, CategoryType::class);
    }

}