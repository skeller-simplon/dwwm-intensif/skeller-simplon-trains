<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Account;
use App\Form\AccountType;

use JMS\Serializer\SerializerInterface;
/**
 * @Route("/api/account", name="account")
 */
class ApiAccountController extends AbstractRestController {

    public function __construct(SerializerInterface $serializer) {
        parent::__construct($serializer, Account::class, AccountType::class);
    }

}