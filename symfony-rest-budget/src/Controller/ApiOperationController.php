<?php


namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Operation;
use App\Form\OperationType;

use JMS\Serializer\SerializerInterface;

/**
 * @Route("/api/operation", name="operation")
 */
class ApiOperationController extends AbstractRestController {

    public function __construct(SerializerInterface $serializer) {
        parent::__construct($serializer, Operation::class, OperationType::class);
    }

}