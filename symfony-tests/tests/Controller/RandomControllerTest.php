<?php 

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class RandomControllerTest extends WebTestCase{
    public function testShowNumber(){
        $this->assertEquals(10, 10);

        $client = static::createClient();
        $crawler = $client->request('get', '/random');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        
        $para = $crawler->filter('h1');
        $this->assertGreaterThan(0, $para->text());
        $this->assertLessThan(11, $para->text());

    }
}