<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use symfony\Component\BrowserKit\Client;
use Doctrine\ORM\Tools\SchemaTool;
use App\DataFixtures\AppFixtures;
use App\Entity\Dog;
use Doctrine\Common\DataFixtures\ReferenceRepository;

class DogControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $manager = $this->client->getContainer()->get('doctrine')->getManager();
        $schemaTool = new SchemaTool($manager);
        $classes = $manager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropSchema($classes);
        $schemaTool->createSchema($classes);
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }
    public function testSomething()
    {
        $crawler = $this->client->request('GET', '/');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertContains('Salut les chiens', $crawler->filter('h1')->text());
        $this->assertSame(5, $crawler->filter('li')->count());
        $this->assertContains('fido0', $crawler->filter('li')->text());
        $this->assertContains('breed0', $crawler->filter('li')->text());
    }
    public function testAddSomething()
    {
        $crawler = $this->client->request('GET', '/add-dog');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('Submit')->form();
        $form['dog[name]'] = 'fido';
        $form['dog[breed]'] = 'corgi';
        $form['dog[age]'] = 7;
        $this->client->submit($form);
        $repo = $this->client->getContainer()->get('doctrine')->getRepository(Dog::class);
        $this->assertSame(6, $repo->count([]));

    }
}
