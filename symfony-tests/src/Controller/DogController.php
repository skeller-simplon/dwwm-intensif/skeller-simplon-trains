<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\DogRepository;
use App\Form\DogType;
use App\Entity\Dog;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class DogController extends AbstractController
{
    /**
     * @Route("/", name="dog")
     */
    public function index(DogRepository $dogRepo)
    {
        return $this->render('dog/index.html.twig', [
            'dogs' => $dogRepo->findAll(),
        ]);
    }
    /**
     * @Route ("/add-dog", name="add_dog")
     */
    public function addDog(Request $request, ObjectManager $objectManager){
        $dog = new Dog;
        $form = $this->createForm(DogType::class, $dog);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $objectManager->persist($dog);
            $objectManager->flush();
            return $this->redirectToRoute("dog");
        }
        return $this->render('dog/create-dog.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
