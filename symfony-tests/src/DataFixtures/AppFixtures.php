<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Dog;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 5; $i++) { 
            $dog = new Dog;
            $dog->setAge($i);
            $dog->setBreed("breed". $i);
            $dog->setName("fido". $i);
            $manager->persist($dog);
        }
        $manager->flush();
    }
}
