# NativeScript VUE

Introduction to native script vue with some simple exercices:
- Counter
- Using accelerator & camera addons
- Small game using accelerometer
- Puissance 4 ?


> A native application built with NativeScript-Vue

## Table of Contents

1. [Project](#project)
2. [Installation](#installation)
3. [Contributing](#contributing)
4. [License](#license)

## Project





## Installation
``` bash
# Install dependencies
npm install

# Build for production
tns build <platform> --bundle

# Build, watch for changes and debug the application
tns debug <platform> --bundle

# Build, watch for changes and run the application
tns run <platform> --bundle
```

## License

Everything I did for [Simplon](https://simplon.co/) is under [cc0-1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.en) license.

**[Back to top](#table-of-contents)**