# Projet Bollywood

Small project for creating movies in relations with actors

## Table of Contents

1. [Project](#project)
2. [Installation](#installation)
3. [Contributing](#contributing)
4. [License](#license)

## Project



## Installation

- composer install
- php bin/console migrations:migrate
- php bin/console server:run

## License

Everything I did for [Simplon](https://simplon.co/) is under [cc0-1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.en) license.

**[Back to top](#table-of-contents)**