<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Movies;
use App\Form\MoviesType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class MovieController extends AbstractController
{

    /**
     * @Route ("/movies", name="movies")
     */
    function First(Request $request)
    {
        if ($request->get("name") !== null && $request->get("date") !== null && $request->get("director") !== null) {
            $movie = new Movies(
                1,
                $request->get("name"),
                $request->get("date"),
                $request->get("director")
            );

            dump($movie->modelData());
            return $this->render("movies.html.twig", [$movie]);
        }
        return $this->render("movies.html.twig", []);
    }
    /**
     * @Route("/movie-form", name="movie-form")
     */
    public function movieForm(Request $request)
    {
        $movies = new Movies();
        $form = $this->createForm(MoviesType::class, $movies);
        $form->add('submit', SubmitType::class, [
            'label' => 'create',
            'attr' => ['class' => 'btn btn-default pull-right']
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $moviesData = $form->getData();
        }
        // Le return moviesData est invalide à cause de ifs
        return $this->render("movies.html.twig", [
            "form" => $form->createView(),
            "moviesData" => $moviesData
        ]);
    }
}
