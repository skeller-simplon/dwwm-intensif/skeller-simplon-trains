import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0,
    total: 0
  },
  mutations: {
    /**
     * Cette methode incrémente la variable en state count, A noter que le return
     * sera rarement utilisé mais peut être utile, d'où l'idée de le mettre par défaut.
     * @param {state} state 
     */
    INCREMENT(state) {
      return state.count++;
    },
    DECREMENT(state, number) {
      // state.total = number + (number - 1);
      // return state.count--;

      // state.count=state.count-3;
      // Plus maintenable puisque décrement en fonction du décrement actuel
      state.count--;
      return state.total = number + state.count;
    }
  },
  actions: {
    countUp({ commit }) {
      commit('INCREMENT');
    },
    countDown({ commit }, number) {
      commit('DECREMENT', number);
    }
  },
  strict: true
})
