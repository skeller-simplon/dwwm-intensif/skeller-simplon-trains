<?php

namespace App\tests\Units;

use App\Entity\User;
use App\Entity\Article;
use PHPUnit\Framework\TestCase;
use App\Service\CalculatorService;


class CalculatorTest extends TestCase
{

    /**
     * @var CalculatorService
     */
    private $service;

    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    private $userAdmin;

    /**
     * @var Article
     */
    private $article;

    public function setUp()
    {
        $this->service = new CalculatorService();

        $userAdmin = new User();
        $userAdmin->setUsername('Admin');
        $userAdmin->setUsername('Admin');
        $userAdmin->setRole('ROLE_ADMIN');
        $this->userAdmin = $userAdmin;
        
        $user = new User();
        $user->setUsername('User');
        $user->setUsername('User');
        $user->setRole('ROLE_USER');
        $this->user = $user;

        $article = new Article();
        $article->setAuthor($user);
        $article->setContent('Bloup bloup blip blup ga bu zo meu');
        $article->setDate(new \DateTime());
        $article->setTitle('TestTitre');
        $article->setImg('coucou');
        $this->article = $article;
        $this->user->addArticles($this->article);
    }

    public function testUsername(){
        // On test le user
        $this->assertSame(4, $this->service->usernameNumberChar($this->user->getUsername()));
        $this->assertTrue($this->service->usernameGoodNumber($this->user->getUsername()));

        // On test l'admin
    }
    public function testAdmin(){
        $this->assertSame(5, $this->service->usernameNumberChar($this->userAdmin->getUsername()));
        $this->assertTrue($this->service->usernameGoodNumber($this->userAdmin->getUsername()));
    }
    public function testUserPostCount(){
        $this->assertSame(1, $this->service->userPostsCount($this->user));
    }
    public function testUsernameNumberWord(){
        $this->assertSame(1, $this->service->usernameNumberWord($this->user->getUsername()));
    }
    public function testIsThisYearDate(){
        $this->assertTrue($this->service->isThisYearPost($this->article));
    }

}
