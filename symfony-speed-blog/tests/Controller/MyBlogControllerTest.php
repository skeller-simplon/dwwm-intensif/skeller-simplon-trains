<?php

namespace App\Tests\Controller;

use App\Entity\Article;


class MyBlogControllerTest extends UserControllerTest
{
    /**
     * @var Client
     */
    public $client;

    public function setUpNew()
    {
        $this->client = static::createClient();
        $this->setUp();
        $this->logInUser();
    }

    public function testAddPost()
    {
        $this->setUpNew();
        
        $crawler = $this->client->request('GET', '/my-blog/add');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $repoPost = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(Article::class);
        $this->assertSame('Add a new post', $crawler->filter('h1')->text());

        $value = $repoPost->count([]);
        $this->assertSame($value, $repoPost->count([]));

        $this->client->submitForm('Submit', [
            'article[title]' => 'Un super article',
            'article[content]' => 'bonjour',
            'article[fileImg]' => 'image',
        ]);

        $this->assertSame($value, $repoPost->count([]));
    }
}
