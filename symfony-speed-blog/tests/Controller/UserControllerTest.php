<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
// use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use App\Entity\User;

class UserControllerTest extends WebTestCase
{
    /**
     * @var Client
     */
    public $client;

    /**
     * @var User
     */
    private $userAdmin;

    /**
     * @var User
     */
    private $userJean;


    public function setUp() {
        $this->client = static::createClient();
        
        //Get le User Repository pour retrouver Jean et Admin
        //Sinon on devrait créer nos Fausses Data
        $repoUser = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);

        //Get User Admin d'id 10
        $this->userAdmin = $repoUser->findOneById(11);
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        //Get User Jean d'id 11
        $this->userJean = $repoUser->findOneById(12);
        $this->assertSame(['ROLE_USER'],$this->userJean->getRoles());
    }

    public function testApp()
    {
        // Politesse
        $this->logInUser();
        $crawler = $this->client->request('GET', '/');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        // Repo de Jean ne fonctionne pas pour moi
        // $repoUser = $this->client->getContainer()->get('doctrine')->getRepository(User::class);
        $repoUser = $this->client->getContainer()->get('doctrine')->getManager()->getRepository(User::class);
        $this->assertSame(12, $repoUser->count([]));
    }

    public function testAuthUserNotAdmin()
    {
        $this->logInUser();
        $crawler = $this->client->request('GET', '/');

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame('Here\'s our blogs', $crawler->filter('h1')->text());
        // $this->assertSame('Welcome User', $crawler->filter('h2')->text());
    }

    public function testAuthUserAdmin()
    {
        $this->logInAdmin();
        $crawler = $this->client->request('GET', '/');
        //On test le user admin
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        $this->assertSame(Response::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame('Here\'s our blogs', $crawler->filter('h1')->text());
        // $this->assertSame('Welcome Admin', $crawler->filter('h2')->text());
    }

    public function logInUser()
    {
        $session = $this->client->getContainer()->get('session');
        $this->assertSame(['ROLE_USER'],$this->userJean->getRoles());

        //Création du Token User Jean avec ROLE_USER
        $token = new UsernamePasswordToken($this->userJean, $this->userJean->getPassword(), 'main', $this->userJean->getRoles());
        
        //Création de la session $_SESSION qui va contenir le token
        $session->set('_security_main', serialize($token));
        $session->save();

        //Création du cookie, qui contient la session, et que l'on donne à notre client
        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

    private function logInAdmin()
    {
        $session = $this->client->getContainer()->get('session');
        $this->assertSame(['ROLE_ADMIN'],$this->userAdmin->getRoles());

        $token = new UsernamePasswordToken($this->userAdmin, $this->userAdmin->getPassword(), 'main', $this->userAdmin->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $this->client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

}