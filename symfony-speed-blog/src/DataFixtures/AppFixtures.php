<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Article;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\File;
use App\Service\UploadService;
use Symfony\Component\Filesystem\Filesystem;
use Faker;

class AppFixtures extends Fixture
{
    private $faker;
    
    private $filesystem;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var UploadService
     */
    private $uploadService;


    public function __construct(UserPasswordEncoderInterface $encoder, UploadService $uploadService, Filesystem $filesystem)
    {
        $this->encoder = $encoder;
        $this->uploadService = $uploadService;
        $this->filesystem = $filesystem;
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
        $this->filesystem->remove($_ENV['UPLOAD_DIRECTORY']);
        if(!is_dir($_ENV['UPLOAD_DIRECTORY'. ""])){
            mkdir($_ENV['UPLOAD_DIRECTORY'. ""]);
        }
        $this->loadUsers($manager);
        $this->loadArticles($manager);

        $manager->flush();
    }
    private function loadUsers(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $user = new User;
            $user->setRole("ROLE_USER");
            $user->setUsername($this->faker->name);
            $user->setPassword($this->encoder->encodePassword($user, $user->getUsername()));
            $manager->persist($user);
            $this->addReference("user".$i, $user);
        }
        $admin = new User;
        $admin->setRole("ROLE_ADMIN");
        $admin->setUsername($this->faker->name);
        $admin->setPassword($this->encoder->encodePassword($admin, $admin->getUsername()));
        $manager->persist($admin);
        $this->addReference("user10", $admin);

        $jean = new User;
        $jean->setRole("ROLE_USER");
        $jean->setUsername("jdmel");
        $jean->setPassword($this->encoder->encodePassword($jean, $jean->getUsername()));
        $manager->persist($jean);
        $this->addReference("user11", $admin);
    }
    private function loadArticles(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $article = new Article;
            $article->setAuthor($this->getReference('user' . rand(0, 12)));
            $article->setContent($this->faker->text);
            
            $this->filesystem->copy(__DIR__ . '/../../assets/fixtures/img1.jpg', __DIR__ . '/../../assets/fixtures/imgtemp.jpg');
            $image = $this->uploadService->upload(new File($this->faker->image()));
            $article->setImg($image);
          

            $article->setTitle(implode(" ",$this->faker->words(rand(3, 15))));
            $article->setDate($this->faker->dateTimeBetween());
            $manager->persist($article);
        }
    }
}
