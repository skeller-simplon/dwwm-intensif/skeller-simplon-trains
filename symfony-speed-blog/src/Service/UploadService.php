<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\Exception\FileException;


class UploadService{
    public function upload(File $file):string{
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        try {
            $file->move($_ENV['UPLOAD_DIRECTORY'], $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }
}