<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\Article;

class CalculatorService
{

    public function usernameNumberChar(string $string): int
    {
        return strlen($string);
    }
    public function usernameNumberWord(string $string): int
    {
        return count(str_word_count($string, 2));
    }
    public function usernameGoodNumber(string $string): bool
    {
        return strlen($string) <= 50;
    }
    public function userPostsCount(User $user): int
    {
        return $user->getArticles()->count();
    }
    public function isThisYearPost(Article $article):bool
    {
        if ($article->getDate()->format('Y') == '2019') {
            return true;
        }
        return false;
    }
}
