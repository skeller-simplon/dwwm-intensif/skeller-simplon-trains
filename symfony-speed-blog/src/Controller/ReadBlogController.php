<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserRepository;
use App\Entity\User;
use App\Repository\ArticleRepository;
use App\Entity\Article;

class ReadBlogController extends AbstractController
{
    /**
     * @Route("/", name="blog")
     */
    public function blog(UserRepository $userRepository, ArticleRepository $articleRepository)
    {
        return $this->render('read_blog/blog.html.twig', [
            'articles' => $articleRepository->findAll(),
            'users' => $userRepository->findAll()
        ]);
    }
    /**
     * @Route("/blog/{user}", name="one_blog")
     */
    public function oneBlog(User $user, ArticleRepository $articleRepository){
        return $this->render("read_blog/one_blog.html.twig",[
            'user' => $user
            // 'articles' => $articleRepository->findBy(['author' => $user->getId()])
        ]);
    }
    /**
     * @Route ("/post/{article}", name="post")
     */
    public function post(Article $article){
        return $this->render("read_blog/post.html.twig",[
            'article' => $article
        ]);
    }
}
