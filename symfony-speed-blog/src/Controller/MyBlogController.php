<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\UploadService;

class MyBlogController extends AbstractController
{
    /**
     * @Route("/my-blog/add/{article}", name="myblog_add")
     */
    public function myBlogAdd(Request $request, ObjectManager $objectManager, UploadService $uploadService, Article $article = null)
    {
        $edit = true;
        if (!$article) {
            $article = new Article;
            $edit = false;
        }
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($this->getUser());
            $article->setDate(new \DateTime());
            if ($article->getFileImg()) {
                $article->setImage($uploadService->upload($article->getFileImage()));
            }
            $objectManager->persist($article);
            $objectManager->flush();
            return $this->redirectToRoute('myblog_manage');
        }
        return $this->render('my_blog/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/my-blog/manage", name="myblog_manage")
     */
    public function myBlogManage()
    {
        return $this->render("my_blog/manage.html.twig", []);
    }
    /**
    * @Route("/my-blog/delete/{article}", name="myblog_delete")
    */
    public function myBlogDelete(Article $article, ObjectManager $objectManager)
    {
        $objectManager->remove($article);
        $objectManager->flush();
        return $this->redirectToRoute("myblog_manage");
    }
}
