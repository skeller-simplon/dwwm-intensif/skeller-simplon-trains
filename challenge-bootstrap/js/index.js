let formButton = document.querySelector('#formButton');
let formEmail = document.querySelector('#email');

formButton.addEventListener('click', function () {
    console.log('MICHEL');
    let email = document.querySelector('#email').value;
    let passwordBase = document.querySelector('#pwd').value;
    let passwordConfirm = document.querySelector('#pwd-confirm').value;
    displayMessage(email, passwordBase, passwordConfirm);
});


formEmail.addEventListener('input', function(){
    if (confirmEmail(formEmail.value)){
        formEmail.classList.remove(":invalid")
        formEmail.classList.add(":valid");
        console.log(formEmail.classList);
    }
    else{
        formEmail.classList.remove(":valid")
        formEmail.classList.add(":invalid");
        email.classList.log()
        console.log(formEmail.classList);

    }
})


function displayMessage(email, passwordBase, passwordConfirm) {
    let alertMessage = 'Merci pour ton submit. ';
    if (confirmEmail(email)) {
        alertMessage += 'Le mail est valide. ';
    }
    else {
        alertMessage += 'Le mail est incorrect. ';
    }
    if (confirmPassword(passwordBase, passwordConfirm)) {
        alertMessage += 'Le mot de passe est valide. ';
    }
    else {
        alertMessage += 'Le mot de passe est incorrect. ';
    }
    alert(alertMessage);
}

// if (email.indexOf("@") != -1 && email.indexOf(".") != -1) {

function confirmEmail(email) {
    //var mailformat = /^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/;
    if (email.indexOf("@") != -1 && email.indexOf(".") != -1 ) {
        return true;
    }
    else {
        return false;
    }
}

function confirmPassword(passwordBase, passwordConfirm) {
    for (var i = 0; i < passwordBase.length; ++i) {
        if (passwordBase[i] !== passwordConfirm[i]) {
            return false;
        }
    }
    return true;
}