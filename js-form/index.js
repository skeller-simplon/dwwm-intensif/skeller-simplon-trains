let addUserButton = document.querySelector('#formConfirm');
let userList = document.querySelector('#userList');

let user = [];
let i = 0;

document.addEventListener('submit', function (event) {
    event.preventDefault();
    let prenom = document.querySelector('#firstNameField');
    let nomFamille = document.querySelector('#lastNameField');
    let age = document.querySelector('#ageField');
    let languageSelector = document.querySelector('#language');

    user[i] = {
        prenom: prenom.value,
        nomFamille: nomFamille.value,
        age: age.value,
        language: languageSelector.options[languageSelector.selectedIndex].value
    }
    i++;
    displayUsers();
});

function displayUsers(){
    userList.innerHTML = '';
    for (const item of user) {
        if (item.prenom != '' && item.nomFamille != '' && item.age != '') {
            let div = document.createElement("div");
            div.className = 'col-md-12 col-lg-6';
            userList.appendChild(div)

            let userDisplay = document.createElement("article");
            let titleName = document.createElement('h4');
            userDisplay.appendChild(titleName);
            titleName.textContent = item.prenom + ' ' + item.nomFamille;

            let paraName = document.createElement('p');
            userDisplay.appendChild(paraName);
            if (item.language == 'Jean') {
                paraName.textContent = "A " + item.age + " ans et adore " + item.language + ".";
            }
            else {
                paraName.textContent = "A " + item.age + " ans et adore le " + item.language + ".";
            }
            div.appendChild(userDisplay);
        }
    }
    console.log(user);
}
