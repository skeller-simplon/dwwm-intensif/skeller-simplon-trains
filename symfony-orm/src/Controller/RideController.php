<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Ride;
use App\Form\RideType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class RideController extends AbstractController
{
    /**
     * @Route("/ride", name="ride")
     */
    public function index()
    {
        return $this->render('ride/index.html.twig', [
            'controller_name' => 'RideController',
        ]);
    }
    /**
     * @Route ("/add-ride/{ride}", name="add_ride")
     */
    public function addRide(Ride $ride = null, Request $request, ObjectManager $objectManager)
    {
        $display = "Edit";
        if (!$ride) {
            $ride = new Ride();
            $display = "Add";
        }
        $form = $this->createForm(RideType::class, $ride);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($ride);
            $objectManager->flush();
            return $this->redirectToRoute("home");
        }
        return $this->render('ride/add-ride.html.twig', [
            "form" => $form->createView(),
            "display" => $display
        ]);
    }
    /**
     * @Route ("delete-ride/{ride}", name="delete_ride")
     */
    public function deleteRide(Ride $ride = null, ObjectManager $objectManager)
    {
        if ($ride) {
            $objectManager->remove($ride);
            $objectManager->flush();
        }
        return $this->redirectToRoute("home");
    }
}
