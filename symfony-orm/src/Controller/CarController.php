<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Car;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\CarType;

class CarController extends AbstractController
{
    /*
     * @Route("/car", name="car")
     */
    public function index()
    {
        return $this->render('car/index.html.twig', [
            'controller_name' => 'CarController',
        ]);
    }
    /**
     * @Route ("/add-car/{car}", name="add_car")
     */
    public function addCar(Request $request, ObjectManager $object, Car $car = null)
    {
        $target = "Edit";
        if (!$car) {
            $car = new Car();
            $target = "Create";
        }
        $form = $this->createForm(CarType::class, $car);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $object->persist($car);
            $object->flush();
            if ($target == "create") {
                return $this->redirectToRoute("home");
            } else {
                return $this->redirectToRoute("display_person", [
                    "person" => $car->getPerson()->getId()
                ]);
            }
        }
        return $this->render("car/car-form.html.twig", [
            "form" => $form->createView(),
            "target" => $target
        ]);
    }
    /**
     * @Route("display-car/{car}", name="display_car")
     */
    public function displayCar(Car $car)
    {
        return $this->render('car/display-car.html.twig', [
            "car" => $car
        ]);
    }
    /**
     * @Route ("delete-car/{car}", name="delete_car")
     */
    public function deleteCar(Car $car = null, ObjectManager $objectManager){
        if ($car) {
            $objectManager->remove($car);
            $objectManager->flush();    
        }
        return $this->redirectToRoute("home");
    }
}
