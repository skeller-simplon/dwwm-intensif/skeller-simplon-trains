<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Category;
use App\Form\CategoryType;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index()
    {
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }

    /**
     * @Route("/add-category/{category}", name="add_category")
     */
    public function addCategory(Category $category = null, Request $request, ObjectManager $objectManager)
    {
        if (!$category){
            $category = new Category;
        }
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($category);
            $objectManager->flush();
            return $this->redirectToRoute("home");
        }
        return $this->render("category/add-category.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    /**
     * @Route("/display-categories", name="display_categories")
     */
    public function displayCategories(CategoryRepository $categoryRepository)
    {
        $categories = $categoryRepository->findAll();
        dump($categories);
        return $this->render('category/display-categories.html.twig', [
            'categories' => $categories,
        ]);
    }
}
