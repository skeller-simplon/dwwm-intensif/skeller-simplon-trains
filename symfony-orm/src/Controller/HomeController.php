<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Person;
use App\Form\PersonType;
use Doctrine\Common\Persistence\ObjectManager;
use App\Repository\PersonRepository;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PersonRepository $repo)
    {
        $persons = $repo->findAll();
        return $this->render('home/index.html.twig', [
            "persons" => $persons
        ]);
    }
    /**
     * @Route("/add-person/{person}", name="add_person")
     */
    public function form(Person $person = NULL, Request $request, ObjectManager $object)
    {
        if (!$person) {
            $person = new Person();
        }
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $object->persist($person);
            $object->flush();
            return $this->redirectToRoute("home");
        }
        return $this->render('home/add-person.html.twig', [
            "form" => $form->createView()
        ]);
    }
    /**
     * @Route("display-person/{person}", name="display_person")
     */
    public function displayPerson(Person $person)
    {
        return $this->render('home/display-person.html.twig', [
            "person" => $person
        ]);
    }
    /**
     * @Route("delete-person/{person}", name="delete_person")
     */
    public function deletePerson(Person $person, ObjectManager $object)
    {
        $object->remove($person);
        $object->flush();
        return $this->redirectToRoute("home");
    }
}
