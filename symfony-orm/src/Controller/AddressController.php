<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Address;
use App\Form\AddressType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use App\Repository\AddressRepository;

class AddressController extends AbstractController
{
    /**
     * @Route("/address", name="address")
     */
    public function index()
    {
        return $this->render('address/index.html.twig', [
            'controller_name' => 'AddressController',
        ]);
    }
    /**
     * @Route ("/add-address/{address}", name="add_address")
     */
    public function addAddress(Address $address = null, SymfonyRequest $request, ObjectManager $objectManager)
    {
        $display = "Edit";
        if (!$address) {
            $address = new Address();
            $display = "Add";
        }
        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $objectManager->persist($address);
            $objectManager->flush();
            return $this->redirectToRoute("home");
        }
        return $this->render('address/add-address.html.twig', [
            "form" => $form->createView(),
            "display" => $display
        ]);
    }
    /**
     * @Route("display-place-to-be", name="display_place_to_be")
     */
    public function displayPlaceToBe(AddressRepository $addressRepository)
    {
        $addresses = $addressRepository->findAll();
        return $this->render("address/display-place-to-be.html.twig", [
            "addresses" => $addresses
        ]);
    }
}
