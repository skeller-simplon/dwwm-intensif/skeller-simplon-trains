<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RideRepository")
 */
class Ride
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $distance;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $passengerNb;

    /**
     * @ORM\Column(type="time")
     */
    private $rideTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Car", inversedBy="rides")
     * @ORM\JoinColumn(nullable=false)
     */
    private $car;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="rides")
     */
    private $startAddress;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="rides")
     */
    private $stopAddress;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPassengerNb(): ?int
    {
        return $this->passengerNb;
    }

    public function setPassengerNb(int $passengerNb): self
    {
        $this->passengerNb = $passengerNb;

        return $this;
    }

    public function getRideTime(): ?\DateTimeInterface
    {
        return $this->rideTime;
    }

    public function setRideTime(\DateTimeInterface $rideTime): self
    {
        $this->rideTime = $rideTime;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getStartAddress(): ?Address
    {
        return $this->startAddress;
    }

    public function setStartAddress(?Address $startAddress): self
    {
        $this->startAddress = $startAddress;

        return $this;
    }

    public function getStopAddress(): ?Address
    {
        return $this->stopAddress;
    }

    public function setStopAddress(?Address $stopAddress): self
    {
        $this->stopAddress = $stopAddress;

        return $this;
    }
}
