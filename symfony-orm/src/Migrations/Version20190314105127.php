<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190314105127 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE address (id INT AUTO_INCREMENT NOT NULL, place VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ride ADD start_address_id INT DEFAULT NULL, ADD stop_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD08778008B FOREIGN KEY (start_address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE ride ADD CONSTRAINT FK_9B3D7CD0C383EF12 FOREIGN KEY (stop_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_9B3D7CD08778008B ON ride (start_address_id)');
        $this->addSql('CREATE INDEX IDX_9B3D7CD0C383EF12 ON ride (stop_address_id)');
        $this->addSql('ALTER TABLE car CHANGE person_id person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE driver CHANGE car_id car_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ride DROP FOREIGN KEY FK_9B3D7CD08778008B');
        $this->addSql('ALTER TABLE ride DROP FOREIGN KEY FK_9B3D7CD0C383EF12');
        $this->addSql('DROP TABLE address');
        $this->addSql('ALTER TABLE car CHANGE person_id person_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE driver CHANGE car_id car_id INT DEFAULT NULL');
        $this->addSql('DROP INDEX IDX_9B3D7CD08778008B ON ride');
        $this->addSql('DROP INDEX IDX_9B3D7CD0C383EF12 ON ride');
        $this->addSql('ALTER TABLE ride DROP start_address_id, DROP stop_address_id');
    }
}
