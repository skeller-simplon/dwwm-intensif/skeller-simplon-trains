<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Ride;
use App\Entity\Person;
use App\Entity\Category;
use App\Entity\Car;
use App\Entity\Address;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $category = new Category;
            $category->setCategoryName("category " . $i);

            $person = new Person();
            $person->setName('name ' . $i);
            $person->setSurname('surname ' . $i);
            $person->setNationality('nationality ' . $i);
            $person->setBirthdate( new \DateTime("2008-11-09"));

            $car = new Car;
            $car->setColor("blue");
            $car->setHorsePower(rand(0, 300));
            $car->setSourcePower("Essence dégueu");
            $car->setModel("Voiture " . $i);
            $car->setPerson($person);
            $car->addCategory($category);

            $address = new Address;
            $address->setPlace("place" . $i);

            $ride = new Ride;
            $ride->setCar($car);
            $ride->setDistance(rand(0, 10000));
            $ride->setPrice(rand(0, 500));
            $ride->setRideTime(new \DateTime("10:00:00"));
            $ride->setStartAddress($address);
            $ride->setStopAddress($address);
            $ride->setPassengerNb($i);

            $manager->persist($category);
            $manager->persist($person);
            $manager->persist($car);
            $manager->persist($address);
            $manager->persist($ride);
        }
        $manager->flush();
    }
}
