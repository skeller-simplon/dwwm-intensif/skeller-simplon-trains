<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\ORM\Tools\SchemaTool;
use App\DataFixtures\AppFixtures;
use App\Entity\Person;
use Symfony\Component\Validator\Constraints\DateTime;

class HomeControllerTest extends WebTestCase
{
    public function setUp(){
        $this->client = static::createClient();
        $manager = $this->client->getContainer()->get('doctrine')->getManager();
        $schemaTool = new SchemaTool($manager);
        $classes = $manager->getMetadataFactory()->getAllMetadata();
        $schemaTool->dropSchema($classes);
        $schemaTool->createSchema($classes);
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testSomething()
    {
        $crawler = $this->client->request('GET', '/add-person');

        $form = $this->client->submitForm('Submit stp', [
            'person[name]' => 'Thierry',
            'person[surname]' => '2Thierry',
            'person[birthdate]' => new \DateTime(date('Y-m-d H:i:s')),
            'person[nationality]' => 'deouf',
        ]);

        $repo = $this->client->getContainer()->get('doctrine')->getRepository(Person::class);
        $this->assertSame(11, $repo->count([]));

        $this->assertSame(302, $this->client->getResponse()->getStatusCode());
    }
}
